<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "transfer".
 *
 * @property int $id
 * @property string $recipient_id
 * @property number $transfer_amount
 */
class Transfer extends \yii\db\ActiveRecord
{

    var $recipient_nickname;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer';
    }

    public function getUserSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    public function getUserRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipient_id']);
    }

    public function usersList()
    {
        $querySender = (new Query())
            ->select(['sender_id', 'send' => 'ifnull(sum(transfer_amount), 0)'])
            ->from('transfer')
            ->groupBy(['t'=> 'sender_id']);

        $queryRecipient = (new Query())
            ->select(['recipient_id', 'received' => 'ifnull(sum(transfer_amount), 0)'])
            ->from('transfer')
            ->groupBy(['r'=> 'recipient_id']);

        return (new Query())
            ->select(['username', 'balance' => 'ifnull(received, 0) - ifnull(send, 0)'])
            ->from('user')
            ->leftJoin(['t' => $querySender], 't.sender_id = user.id')
            ->leftJoin(['r'=>$queryRecipient],'r.recipient_id = user.id');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transfer_amount', 'recipient_nickname'], 'required'],
            [['transfer_amount'], 'number', 'numberPattern' => '/^[0-9]*([\.][0-9]{1,2})?$/',
                'message' => 'Transfer Amount must be in format like 123.45', 'min' => 0],
            [['recipient_nickname'], 'string', 'max' => 255],
            ['recipient_nickname', 'compare', 'compareValue' => Yii::$app->user->getIdentity()->username,
                'operator' => '!=', 'type' => 'string',
                'message' => 'You can not make a transfer for themselves'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipient_id' => 'Recipient Id',
            'transfer_amount' => 'Transfer Amount',
        ];
    }
}
