<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transfer`.
 */
class m171024_171606_create_transfer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transfer', [
            'id' => $this->primaryKey(),
            'sender_id'=>$this->integer(),
            'recipient_id'=>$this->integer(),
            'transfer_amount'=>$this->decimal(10,2)
        ]);

        $this->createIndex('sender_id_index','transfer','sender_id');
        $this->createIndex('recipient_id_index','transfer','recipient_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('transfer');
    }
}
