<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171024_171222_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username'=>$this->string()->notNull()->unique(),
            'auth_key'=>$this->string(60),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
