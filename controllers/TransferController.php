<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use app\models\Transfer;
use yii\web\Controller;
use yii\data\ActiveDataProvider;


class TransferController extends Controller
{


    public function actionOperation()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("user/login"));
        }
        $this->layout = 'operations_layout';

        $user = Yii::$app->user->getIdentity();

        $query = (new \yii\db\Query());
        $query->select(['s_user.username AS sender', 'r_user.username AS recipient', 'transfer.transfer_amount AS amount'])
            ->from('transfer')
            ->innerJoin('user AS s_user', 'transfer.sender_id = s_user.id')
            ->innerJoin('user AS r_user', 'transfer.recipient_id = r_user.id')
            ->where(['sender_id' => $user->id])->orWhere(['recipient_id' => $user->id]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('operation',
            ['provider' => $provider]);

    }


    public function actionNicknamesList()
    {
        $this->layout = 'users_list';
        $model = new Transfer();

        $query = $model->usersList();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('nicknames-list', ['provider' => $provider,]);

    }

    public function actionRemittance()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("user/login"));
        }

        $this->layout = 'remittance_layout';
        $model = new Transfer();
        $recipient = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->sender_id = Yii::$app->user->getIdentity()->id;

            $recipient->register($model->recipient_nickname);
            $model->recipient_id = User::findByUsername($model->recipient_nickname)->id;

            if ($model->save()) {
                return $this->redirect(['operation', 'id' => $model->id]);
            }
        }

        return $this->render('remittance', [
            'model' => $model,

        ]);
    }


}
