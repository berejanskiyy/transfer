<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\web\Controller;


class UserController extends Controller
{


    public function actionLogin()
    {
        $this->layout = 'login';

        $modelLoginForm = new LoginForm();
        if ($modelLoginForm->load(Yii::$app->request->post()) && $modelLoginForm->login()) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("transfer/remittance"));
        }

        // move to login page
        return $this->render('login', ['modelLoginForm' => $modelLoginForm]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(\Yii::$app->urlManager->createUrl("user/login"));
    }
}
