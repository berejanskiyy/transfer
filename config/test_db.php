<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'mysql:host=172.17.0.1;port=3316;dbname=testdb';

return $db;
