<?php


use app\models\LoginForm;

class LoginTest extends \Codeception\Test\Unit
{
    public $appConfig = '@tests/unit/_config.php';
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testLoginNoUser()
    {
        $model = new LoginForm([
            'username' => 'Anton',
        ]);

        expect_that($model->login());
    }

    public function testLoginUser()
    {
        $model = new LoginForm([
            'username' => 'Dima',
        ]);

        expect_that($model->login());
    }


}