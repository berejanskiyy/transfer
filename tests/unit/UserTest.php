<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(7));
        expect($user->username)->equals('Dima');

        expect_not(User::findIdentity(999));
    }


    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('Dima'));
        expect_not(User::findByUsername('not-Dima'));
    }


}
